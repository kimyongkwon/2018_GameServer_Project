#include <windows.h>
#include <stdio.h>

#define THREADCOUNT 4 

HANDLE ghWriteEvent;
HANDLE ghThreads[THREADCOUNT];

DWORD WINAPI ThreadProc(LPVOID);

void CreateEventsAndThreads(void)
{
    int i;
    DWORD dwThreadID;

	// 메인 스레드가 공유버퍼에 값을 쓰는 동안에 
	// 다른 스레드가 접근하지 못하도록 이벤트를 만든다
	// 메인쓰레드는 write만 보조스레드는 read만 한다
    ghWriteEvent = CreateEvent(
        NULL,              
        TRUE,              
        FALSE,             
        TEXT("WriteEvent") 
        );

	// Read만 하는 보조스레드를 4개 만든다.
	for (i = 0; i < THREADCOUNT; i++)
	{
		ghThreads[i] = CreateThread(
			NULL,              
			0,                 
			ThreadProc,        
			NULL,              
			0,                 
			&dwThreadID);
	}
}

void WriteToBuffer(VOID)
{
	// 메인스레드가 2초동안 공유버퍼에 쓴다고 가정한다.
    printf("Main thread writing to the shared buffer...\n");
    Sleep( 2000 );
	SetEvent(ghWriteEvent);
}

void CloseEvents()
{
    CloseHandle(ghWriteEvent);
}

int main(void)
{
    DWORD dwWaitResult;

    CreateEventsAndThreads();

    WriteToBuffer();

	// 2초뒤에 메인스레드는 보소스레드들이 끝나길 기다린다
    printf("Main thread waiting for threads to exit...\n");

	// 모든 스레드이벤트들이 signal되길 기다린다.
	// 세번째 인자가 TRUE이기때문에... 
	// FALSE면 쓰레드 이벤트들 중에 하나만 signal되도 실행된다
    dwWaitResult = WaitForMultipleObjects(
        THREADCOUNT,  
        ghThreads,    
        TRUE,         
        INFINITE);

    switch (dwWaitResult)
    {
    case WAIT_OBJECT_0:
        printf("All threads ended, cleaning up for application exit...\n");
        break;
    default:
        printf("WaitForMultipleObjects failed (%d)\n", GetLastError());
        return 1;
    }
    CloseEvents();

    return 0;
}

DWORD WINAPI ThreadProc(LPVOID lpParam)
{
    UNREFERENCED_PARAMETER(lpParam);

    DWORD dwWaitResult;

    printf("Thread %d waiting for write event...\n", GetCurrentThreadId());

    dwWaitResult = WaitForSingleObject(
        ghWriteEvent, 
        INFINITE);    

	// 이 밑 부분을 Read하는 부분이라고 가정한다.
    switch (dwWaitResult)
    {
    case WAIT_OBJECT_0:
        printf("Thread %d reading from buffer\n",
            GetCurrentThreadId());
        break;

    default:
        printf("Wait error (%d)\n", GetLastError());
        return 0;
    }

    printf("Thread %d exiting\n", GetCurrentThreadId());
    return 1;
	// 스레드가 종료되는 순간 이벤트가 signal된다.
}