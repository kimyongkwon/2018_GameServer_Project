//#include <iostream>
//#include <boost/shared_ptr.hpp>
//
//int main()
//{
//	boost::shared_ptr<int>	spInt;
//	spInt.reset(new int(1));
//	std::cout << *spInt << std::endl;
//}

//#include <boost/bind.hpp>
//#include <vector>
//#include <algorithm>
//#include <iostream>
//
//void print(std::ostream *os, int i)
//{
//	*os << i << '\n';
//}
//
//void Print(int a, int b)
//{
//	printf("%d %d \n", a, b);
//}
//
//int main()
//{
//	// placeholder
//	boost::bind(Print, _2, _1) (3, 9);
//
//	std::vector<int> v{ 1,3,2 };
//	std::for_each(v.begin(), v.end(), boost::bind(print, &std::cout, _1));
//
//}

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <queue>
#include "KCriticalSection.h"

struct KPacket;
typedef boost::shared_ptr<KPacket> KPacketPtr;
struct KPacket
{
};

class KQueue
{
public:
	void AddPacket(KPacketPtr spPacket)
	// AddPacket함수에 들어왔을 때 EnterCriticalSection()이 호출되고
	// 함수를 나가기전에 LeaveCriticalSection()이 호출하니까 thread-safe하게 다음 블록을 보호하게 될 것이다.
	{
		KCriticalSectionLock lock(m_csQueue);
		m_queue.push(spPacket);
	}
	
	template<typename FUNCTOR>
	void ProcessAllPacket(FUNCTOR callback)
	{
		while (m_queue.empty() == false)
		{
			KPacket spPacket;
			CSLOCK(m_csQueue)
			{
				KCriticalSectionLock lock(m_csQueue);
				spPacket = m_queue.front();
				m_queue.pop();
			}
			callback(spPacket);
		}
	}
private:
	std::queue<KPacketPtr> m_queue;
	KCriticalSection	   m_csQueue;
};

class KSession
{
public:
	void AddPacket(KPacketPtr spPacket)
	{
		m_kQueue.AddPacket(spPacket);
	}
	void Update()
	{
		m_kQueue.ProcessAllPacket( boost::bind(&KSession::OnPacket, this, _1) );
	}
	virtual void OnPacket(KPacketPtr spPacket)
	{
		printf("%s\r\n", __FUNCTION__);
	}
private:
	KQueue m_kQueue;
};

int main()
{
	KPacketPtr spPacket;
	spPacket.reset(new KPacket());

	KSession session;
	session.AddPacket(spPacket);
	session.AddPacket(spPacket);

	session.Update();
}
