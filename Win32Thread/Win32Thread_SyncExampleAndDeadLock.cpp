#include <windows.h>
#include <stdio.h>
#include <iostream>

HANDLE ghEvents[2];
int g_value = 1;

DWORD WINAPI ThreadProc(LPVOID);

int main(void)
{
    HANDLE hThread;
    DWORD i, dwEvent, dwThreadID;

	// 이벤트를 2개 만든다.
    for (i = 0; i < 2; i++)
    {
        ghEvents[i] = CreateEvent(
            NULL,   
            FALSE,  
            FALSE,  
            NULL);  
    }

    // 보조 스레드 생성
    hThread = CreateThread(
        NULL,         
        0,            
        (LPTHREAD_START_ROUTINE)ThreadProc,
        NULL,         
        0,            
        &dwThreadID); 

	while (g_value < 10) {  
        std::cout << g_value << " : MainThread" << std::endl;
        g_value += 1;
        Sleep( 1000 );				
        SetEvent( ghEvents[1] );	
		WaitForSingleObject(
			ghEvents[0],
			INFINITE);
    }

    // Close event handles
    for (i = 0; i < 2; i++)
        CloseHandle(ghEvents[i]);

    return 0;
}

// 보조 스레드
DWORD WINAPI ThreadProc(LPVOID lpParam)
{
    while (g_value < 10) {
		WaitForSingleObject(
			ghEvents[1],     
			INFINITE);       
        std::cout << g_value << " : SubThread" << std::endl;
        g_value += 1;
        Sleep( 1000 );
        SetEvent( ghEvents[0] );	
    }
    return 0;
}
