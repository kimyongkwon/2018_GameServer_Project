// Module Name: tcpserver.cpp
//
// Description:
//
//    This sample illustrates how to develop a simple TCP server application
//    that listens for a TCP connection on port 5150 and receives data. This 
//    sample is implemented as a console-style application and simply prints 
//    status messages when a connection is accepted and when data is received 
//    by the server.
//
// Compile:
//
//    cl -o tcpserver tcpserver.cpp ws2_32.lib
//
// Command Line Options:
//
//    tcpserver.exe
//
//    NOTE: There are no command parameters. 
//


#include <winsock2.h>
#include <stdio.h>

#pragma comment(lib, "ws2_32.lib")

void main(void)
{
   WSADATA              wsaData;
   SOCKET               ListeningSocket;
   SOCKET               NewConnection;
   SOCKADDR_IN          ServerAddr;
   SOCKADDR_IN          ClientAddr;
   int                  ClientAddrLen = sizeof(ClientAddr);
   int                  Port = 5150;
   int                  Ret;
   char                 DataBuffer[1024];
   
   // WSAStartup함수는 프로세스에 의해 WinsockDLL 사용을 개시한다.
   if ((Ret = WSAStartup(MAKEWORD(2,2), &wsaData)) != 0){
      printf("WSAStartup failed with error %d\n", Ret);
      return;
   }
 
   // server socket을 만든다. 
   // 첫번째 인자에는 IPv4를 사용하기 떄문에 AF_INET를 쓴다.
   // 두번째 인자에는 TCP를 사용하기 떄문에 SOCK_STREAM을 쓴다.
   // 세번째 인자는 TCP를 사용하기 때문에 IPPROTO_TCP
   // 성공시 socket()함수는 새 소켓을 반환
   ListeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

   ServerAddr.sin_family = AF_INET;
   ServerAddr.sin_port = htons(Port);				// htons 빅 엔디안으로 변환
   ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// IP주소

   // 첫번째 인자에 socket()함수에서 리턴된 소켓을 전달
   // 두번쨰 인자에는 sockaddr_in의 구조체의 포인터를 전달한다.
   // 세번째 인자는 구조체의 크기(바이트단위)를 전달한다
   bind(ListeningSocket, (SOCKADDR *)&ServerAddr, sizeof(ServerAddr));

   // bind함수가 성공하게 되면 ListeningSocket은 주어진 인터넷주소와 포트와 결합하게 되고
   // ListeningSocket을 사용할 수 있게 된다.
   
   // 이제부터 서버는 클라이언트의 통신 요청을 기다려야한다. 
   // 이 일은 listen()이라는 함수를 통해 가능하다.
   // 첫번째 인자는 클라이언트의 연결 요청을 받아들이는 역할을 하게 될 소켓(ListeningSocket)을 전달한다
   // 두번째 인자는 연결요청대기큐(queue)의 크기를 나타낸다.
   // 만약 인자로 5가 되면 큐의 크기가 5가 되어 클라이언트의 연결요청을 5개까지 대기시킬수있게 된다.
   // SOMAXCONN으로 설정되면 요청받을 수 있는 최대 값으로 설정한다.
   listen(ListeningSocket, SOMAXCONN);
   printf("We are awaiting a connection on port %d.\n", Port);
   // listen함수를 성공적으로 호출하고 이제는 클라이언트가 서버에 접속할때까지 계속 기다린다.

   // 클라이언트가 접속(connect)하게 되면 서버는 클라이언트의 접속을 수용한다.
   // 접속을 받아드리는 함수는 accept()함수이다.
   // accept()는 socket()호출시, 두번째 인자(type)가 SOCK_STREAM, SOCK_SEQPACKET과 같은 connet-based socket이여만 한다.
   // 첫번째 인자에는 socket()함수를 통하여 생성된 socket을 전달한다
   // 두번째 인자에는 접속한 client주소정보를 저장한다. 
   // 세번째 인자에는 접속한 client주소정보의 크기를 저장한다.
   // accept()함수 호출이 성공하면 client와 연결된 새로운 socket이 생성되어 리턴한다.
   NewConnection = accept(ListeningSocket, (SOCKADDR *) &ClientAddr,&ClientAddrLen);
   printf("We successfully got a connection from %s:%d.\n",
          inet_ntoa(ClientAddr.sin_addr), ntohs(ClientAddr.sin_port));

   // 현재 구조에서는 클라이언트를 하나만 접속하는 구조이다.
   
   closesocket(ListeningSocket);

   printf("We are waiting to receive data...\n");

   // 연결된 소켓으로부터 데이터를 수신할 수 있다.
   // 이 떄 사용하는 함수는 recv()함수
   // 첫번째 인자는 accept()로 연결된 socket(NewConnection)을 전달한다.
   // 두번쨰 인자는 데이터를 수신하여 저장할 버퍼를 전달한다
   // 세번째 인자는 버퍼의 크기
   // 네번째 인자는 읽을 데이터 유형 또는 읽는 방법에 대한 옵션을 나타낸다. 
   // flags의 값이 0이면 일반데이터를 수신한다.
   Ret = recv(NewConnection, DataBuffer, sizeof(DataBuffer), 0);
   
   printf("We successfully received %d byte(s).\n", Ret);   
   printf("We are now going to close the client connection.\n");
   closesocket(NewConnection);
   WSACleanup();
}
